namespace Inheritance {
  interface Matchable {
    match<C>(pattern: Record<string, (...args: any[]) => C>): C;
  }

  type EitherPattern<A, B, C> = {
    Left: (value: A) => C;
    Right: (value: B) => C;
  };

  abstract class Either<A, B> implements Matchable {
    protected constructor() {}

    public static of<A, B>(value: B) {
      return new Right<A, B>(value);
    }

    public abstract match<C>(pattern: EitherPattern<A, B, C>): C;
  }

  class Left<A, B> extends Either<A, B> {
    constructor(private value: A) {
      super();
    }

    public match<C>(pattern: EitherPattern<A, B, C>): C {
      return pattern.Left(this.value);
    }
  }

  class Right<A, B> extends Either<A, B> {
    constructor(private value: B) {
      super();
    }

    public match<C>(pattern: EitherPattern<A, B, C>): C {
      return pattern.Right(this.value);
    }
  }

  const validate = (s: string): Either<Error, string> => {
    if (s.length > 2) return new Right(s);
    else return new Left(new Error('Too short'));
  };

  const e = Either.of<Error, number>(2);

  // const b = validate('qwe').match({
  //   Right: x => x + 'Good!',
  //   Left: x => x.message + 'Bad!',
  // });

  const b = validate('qwe').match({
    Left: x => '2',
    Right: x => null,
  });

  //   {
  //   Right: x => x + 'Good!',
  //   Left: x => x.message + 'Bad!',
  // });
}
