namespace Union {
  interface Matchable {
    match<T>(pattern: Record<string, (...args: unknown[]) => T>): T;
  }

  type EitherPattern<A, B, C> = {
    Left: (value: A) => C;
    Right: (value: B) => C;
  };

  type Either<A, B> = Left<A, B> | Right<A, B>;

  class Left<A, B> implements Matchable {
    constructor(private value: A) {}

    public match<C>(pattern: EitherPattern<A, B, C>): C {
      return pattern.Left(this.value);
    }
  }

  class Right<A, B> implements Matchable {
    constructor(private value: B) {}

    public match<C>(pattern: EitherPattern<A, B, C>): C {
      return pattern.Right(this.value);
    }
  }

  const validate = (s: string): Either<Error, string> => {
    if (s.length > 2) return new Right(s);
    else return new Left(new Error('Too short'));
  };

  const b = validate('qwe').match({
    Right: x => x + 'Good!',
    Left: x => x.message + 'Bad!',
  });
}
