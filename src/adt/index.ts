// union

// type Pattern<P> = {

// }

// P — pattern?
// interface ADT<U> {
// 	match<T>(p: )
// }

// inheritance
class A {}
class B extends A {}
class C extends A {}

// type APattern = {
// 	B:
// }

// interface Matchable<X> {
// 	match(p: )
// }

type X = Y | Z;
class Y /* implements Matchable<X> */ {
}
class Z {}

type XPattern<A> = {
  Y: <B>(b: B) => A;
  Z: <C>(c: C) => A;
};



// type Pattern<U> = {
// 	[K in U["constructor"]["name"]]: string
// };

// const p: Pattern<X> = {
// 	a: "2",
// }

// type APattern = {
// 	B:
// }
