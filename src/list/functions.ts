import { List } from '.';

const tail = <A>(l: List<A>): List<A> => {
  return null as any;
};

const l = List.from([1, 2, 3]);

console.log(tail(l));

// // List 1, 2, 3

// const l = new Cons<number>(
//   1,
//   new Cons<number>(2, new Cons<number>(3, new Nil<number>()))
// );

// const b = l.match({
//   Nil: () => 'Empty list',
//   Cons: (head, tail) => head.toString(),
// });
