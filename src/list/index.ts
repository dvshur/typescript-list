export interface Matchable {
  match<C>(pattern: Record<string, (...args: any[]) => C>): C;
}

export type ListPattern<A, B> = {
  Nil: () => B;
  Cons: (head: A, tail: List<A>) => B;
};

export abstract class List<A> implements Matchable {
  protected constructor() {}

  public static from<A>(array: A[]): List<A> {
    return [...array]
      .reverse()
      .reduce((acc, x) => new Cons(x, acc), new Nil<A>());
  }

  public abstract match<B>(pattern: ListPattern<A, B>): B;
}

export class Nil<A> extends List<A> {
  constructor() {
    super();
  }

  public match<B>(pattern: ListPattern<A, B>): B {
    return pattern.Nil();
  }
}

export class Cons<A> extends List<A> {
  constructor(private readonly head: A, private readonly tail: List<A>) {
    super();
  }

  public match<B>(pattern: ListPattern<A, B>): B {
    return pattern.Cons(this.head, this.tail);
  }
}
